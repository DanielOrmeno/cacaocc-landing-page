<?php

/*
| 
*/
if (isset($_POST)) {
  
  //- Form Validation Variables
  $formValid = true;
  $errors = array();
  
  //- Submission data Varibles
  $ipaddress = $_SERVER['REMOTE_ADDR'];
  $date = date('d/m/y');
  $time = date('H:i:s');
  
  //- form data Variables
  $name = $_POST['name'];
  $email = $_POST['email'];
  $message = $_POST['message'];
  $humanVerification = $_POST['humanVerification'];
  
  //- Form Validation 
  if (empty($name)) {
    $formValid = false;
    $errors [] = "Please enter your name";
  } 
  
  if (empty($email)) {
    $formValid = false;
    $errors[] = "Please enter  your email address";
  } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
    $formValid = false;
    $errors [] = "Please enter a valid email address";
  }
  
  if (empty($message)) {
    $formValid = false;
    $errors [] = "Please enter a message";
  }
  
  if (empty($humanVerification)) {
    $formValid = false;
    $errors[] = "Please enter an answer so we know you are not a robot!";
  }  
  
  if ($humanVerification != 4) {
    $formValid = false;
    $errors[] = "Incorrect answer, please try again!";
  }
  
  if ($formValid) {
    $headers = "From: dormenog@me.com" . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1'. "\r\n";
    
    $emailBody = "<p>You received a new message from the Cacao Cupcakes website.</p>
                  <p><strong>Name:</strong>{$name}</p>
                  <p><strong>Email:</strong>{$email}</p>
                  <p><strong>Message</strong>{$message}</p>";
                  /*<p>This message was sent from the IP address: {$ipAddress} on {$date} at {$time}</p>";*/
    
    mail("dormenog@gmail.com", "New Enquiry", $emailBody, $headers);
  }
  
  //- Array that store variables to be used when redirecting the user to the form.
  $returnData = array (
    'posted_form_data' => array('name' => $name, 'email' => $email, 'message' => $message),
    'valid_form' => $formValid,
    'errors' => $errors
  );
  
  //- Check if its not an AJAX request
  if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
    
    //- Set session variables
    session_start();
    $_SESSION['cf_returndata'] = $returnData;
    
    //- redirect back to form
    header('location:'. $_SERVER['HTTP_REFERER']);
  }
} 

?>