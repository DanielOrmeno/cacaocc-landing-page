<!Doctype html>
<head>
  <title>Cacao Cupcakes</title>
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css">
  <link href="css/custom-theme.css" rel="stylesheet" type="text/css">
  <!-- Google's JQuery CDN -->
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <!-- FullPage plugin -->
  <link rel="stylesheet" type="text/css" href="fpPlugin/jquery.fullPage.css" />
  <script type="text/javascript" src="fpPlugin/jquery.fullPage.js"></script>
  <!-- JQuery doc ready script -->
  <script type="text/javascript" src="js/fullscreen.js"></script>
</head>
<body>
  <div id="fullpage">
    <div class="section">
      <?php include ('underConstruction.php'); ?>
    </div>
    <div class="section">
      <?php include ('contact.php'); ?>
    </div>
  </div>
</body>

