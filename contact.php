<div class="row">
  <div class="container-fluid contactHeader">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <h1> CONTACT US </h1>
    </div>
  </div>
</div>
<div class=row>
  <!-- Contact form -->
  <div class="col-sm-12 col-md-8 col-lg-8 ccc-form">
    <?php include ('contactForm.php'); ?>
  </div>
  <!-- Social media -->
  <div class="col-sm-12 col-md-4 col-lg-4 ccc-social">
    <div class="row">
      <h4 class="headingCenter">CONTACT</h4>
      <h5>Cacao Cupcakes</h5>
      <h5>Gold Coast, QLD</h5>
      <h5>Tel: +6140000000</h5>
      <h5>Email: info@cacaocupcakes.com</h5>
      
      <hr class="col-sm-8 col-md-8 col-lg-8">
        
    </div>
    <div class="row">
      <div class="col-md-4 col-sm-4 col-xs-4">
        <img src="res/icons/facebookCS2.png" class="img-responsive centerImg">
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4">
        <img src="res/icons/twitterCS2.png" class="img-responsive centerImg">
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4">
        <img src="res/icons/instagramCS2.png" class="img-responsive centerImg">
      </div>
    </div>
  </div>
</div>