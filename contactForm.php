<?php session_start();?>
<?php
  $cf = array();
  $sr = false;
  
  if (isset($_SESSION['cf_returndata'])) {
    $cf = $_SESSION['cf_returndata'];
    $sr = true;
  }
?>
<form class="form-horizontal" role="form" method="post" action="contactScript.php">
  <div class="form-group">
    <label for="name" class="col-sm-4 control-label">Your name</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="name" name="name" placeholder="first and last name" required="required">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-4 control-label">Your email</label>
    <div class="col-sm-8">
      <input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" required="required">
    </div>
  </div>
  <div class="form-group">
    <label for="message" class="col-sm-4 control-label">Message</label>
    <div class="col-sm-8">
      <textarea class="form-control" rows="4" id="message" name="message" required="required"></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="humanVerification" class="col-sm-4 control-label">2 + 2 =</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="humanVerification" name="humanVerification" placeholder="are you human?" required="required">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-8 col-sm-offset-4">
      <input id="submit" name="submit" type="submit" value="send" class="btn sendbtn btn-custom">
    </div>
  </div>
</form>
<div class="col-sm-8 col-sm-offset-4">
  <p id="success" class="<?php echo ($sr && $cf['valid_form']) ? 'visible' : ''; ?>">Thank you for your enquiry, we'll get back to you shortly!</p>
<ul id="errors" class="<?php echo ($sr && !$cf['valid_form']) ? 'visible' : ''; ?>">
    <li id="info">There were some problems with your form submission:</li>
    <?php 
    if(isset($cf['errors']) && count($cf['errors']) > 0) :
        foreach($cf['errors'] as $error) :
    ?>
    <li><?php echo $error ?></li>
    <?php
        endforeach;
    endif;
    ?>
</ul>
</div>
<?php unset($_SESSION['cf_returndata']); ?>
